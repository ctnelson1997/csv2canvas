import { waitForDebugger } from "inspector";
import { group } from "node:console";

const nfetch = require('node-fetch');
const fs = require('fs');
const CsvReadableStream = require('csv-reader');
var url = require('url');

const COURSE_URL = 'https://canvas.wisc.edu/api/v1'
const COURSE_ID = '83960000000242591'
const ASSIGNMENT_ID = '83960000001150541'
const SECRET = fs.readFileSync('./includes/token.secret')

fetchSubmissions();

function fetchSubmissions(): any {
    let courseUrl = `${COURSE_URL}/courses/${COURSE_ID}/assignments/${ASSIGNMENT_ID}/submissions/?include[]=group&include[]=user&per_page=100`;
    let submissions: any[] = []
    console.log(`Beginning download...`)
    nfetch(courseUrl + '&page=1',
        {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + SECRET
            }
        })
        .then((res: any) => res.json())
        .then((res: any[]) => {
            console.log(`First page downloaded...`)
            submissions = submissions.concat(res);
            nfetch(courseUrl + '&page=2',
                {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Bearer ' + SECRET
                    }
                })
                .then((res: any) => res.json())
                .then((res: any[]) => {
                    submissions = submissions.concat(res);
                    console.log(`Second page downloaded! Processing ${submissions.length} submissions!`);
                    processSubmissions(submissions)
                })
        })
}

function processSubmissions(submissions: any[]) {
    let students: Array<Student> = [];
    submissions.forEach(submission => {
        if(submission && submission.group && submission.group.name) {
            let fullGroupName= submission.group.name
            let groupNum = fullGroupName.substr(fullGroupName.lastIndexOf(' ') + 1)
            let student = new Student(submission.user.id, submission.user.name, groupNum);
            students.push(student);
        } else {
            console.error(`Could not find a group for ${submission.user.name}. Ignoring!`)
        }
    });

    console.log(`Found ${students.length} students!`);
    students.forEach(student => console.log(student.toString()));
    let groups: Group[] = Group.asGroups(students);
    console.log(`Found ${groups.length} groups!`);
    groups.forEach(group => console.log(group.toString()));
    console.log("Beginning grade processing...");
    assignGradesFromCSV(students, groups);
}

function assignGradesFromCSV(students: Student[], groups: Group[]) {
    let gradedStudents: GradedStudent[] = [];
    let inputStream = fs.createReadStream('./includes/grades.csv', 'utf8');
    inputStream
        .pipe(new CsvReadableStream({ parseNumbers: true, trim: true }))
        .on('data', function (row: any) {
            groups.forEach(group => {
                if(group.id === row[CSVMapper.CSV_GROUP_COL].toString()) {
                    let grades: Grade[] = [];
                    CSVMapper.CSV_COL_TO_RUBRIC_ID.forEach((rubricItem: RubricItem, i: number) => {
                        let startIndex = CSVMapper.CSV_GRADE_START + (2 * i);
                        let grade = row[startIndex];
                        let comment = row[startIndex + 1];
                        grades.push(new Grade(rubricItem, comment, grade));
                    });
                    group.students.forEach(student => {
                        gradedStudents.push(new GradedStudent(student, grades));
                    });
                }
            })
        })
        .on('end', function (data: any) {
            console.log('No more rows!');
            submitGrades(gradedStudents);
        });
}

async function submitGrades(gradedStudents: GradedStudent[]) {
    console.log(`Grading complete! ${gradedStudents.length} students graded. See results below.`);
    gradedStudents.forEach(gradedStudent => console.log(gradedStudent.toString()));

    console.log(`${gradedStudents.length} URLs created. See results below.`);
    gradedStudents.forEach(gradedStudent => console.log(gradedStudent.asURL()));

    /** BEGIN POINT OF NO RETURN - SUBMIT GRADES **/
    /** KEEP COMMENTED UNTIL READY TO USE */

    for(let gradedStudent of gradedStudents) {
        console.log(`Grade for ${gradedStudent.student.name} en route!`);

        await nfetch(gradedStudent.asURL(),
        {
            method: 'PUT',
            headers: {
                'Authorization': 'Bearer ' + SECRET
            }
        })
        .then((res: any) => {
            console.log(`Recieved ${res.status} for ${gradedStudent.student.name}`);
            return res.json()
        })
        .then((res: any[]) => { });

        // Stop Canvas Police, assume min wait time of 3 secs and add random offset. 🕵️
        wait(3 + (Math.random() * 3));
    }
}

class RubricItem {
    public rubricId: string;

    public constructor(rubricId: string) {
        this.rubricId = rubricId;
    }

    public toString(): string {
        return `RID ${this.rubricId}`;
    }
}

class CSVMapper {

    public static readonly CSV_GROUP_COL = 0;

    public static readonly CSV_GRADE_START = 1;

    public static readonly CSV_COL_TO_RUBRIC_ID = [
        new RubricItem('_1121'),
        new RubricItem('_6805'),
        new RubricItem('_6899'),
        new RubricItem('_4597'),
        new RubricItem('_5809'),
        new RubricItem('_167'),
        new RubricItem('_2369')
    ]
}

class Grade {
    public rubricItem: RubricItem;
    public comment: string;
    public grade: number;

    public constructor(rubricItem: RubricItem, comment: string, grade: number) {
        this.rubricItem = rubricItem;
        this.comment = comment;
        this.grade = grade;
    }

    public toString(): string {
        return `${this.rubricItem.toString()}: ${this.grade} - ${this.comment}`;
    }
}

class Student {
    public id: number;
    public name: string;
    public group: string;

    public constructor(id: number, name: string, group: string) {
        this.id = id;
        this.name = name;
        this.group = group;
    }

    public toString(): string {
        return `G${this.group} U ${this.id} - ${this.name}`;
    }
}

class Group {
    public id: string;
    public students: Student[];

    public constructor(id: string, students: Student[]) {
        this.id = id;
        this.students = students;
    }

    public static asGroups(students: Student[]): Group[] {
        let groups: Array<Group> = [];
        let assignments = new Map<string, Student[]>();
        students.forEach(student => {
            if(!assignments.has(student.group)) {
                assignments.set(student.group, []);
            }
            assignments.get(student.group)?.push(student);
        });
        assignments.forEach((students: Student[], groupNum: string, map) => {
            groups.push(new Group(groupNum, students));
        });
        return groups;
    }

    public toString(): string {
        let ret = `G${this.id}`;
        this.students.forEach(student => ret += `\n - ${student.toString()}`);
        return ret;
    }
}

class GradedStudent {
    public student: Student;
    public grades: Grade[];

    public constructor(student: Student, grades: Grade[]) {
        this.student = student;
        this.grades = grades;
    }

    public asURL(): string {
        let ret = `${COURSE_URL}/courses/${COURSE_ID}/assignments/${ASSIGNMENT_ID}/submissions/${this.student.id}?`
        this.grades.forEach(grade => {
            ret += `rubric_assessment[${encodeURI(grade.rubricItem.rubricId)}][points]=${grade.grade}&rubric_assessment[${encodeURI(grade.rubricItem.rubricId)}][comments]=${encodeURI(grade.comment)}&`
        });
        return ret.substring(0, ret.length - 1);
    }

    public toString(): string {
        let ret = `${this.student.toString()}`;
        this.grades.forEach(grade => ret += `\n - ${grade.toString()}`);
        return ret;
    }
}

// https://stackoverflow.com/questions/14249506/how-can-i-wait-in-node-js-javascript-l-need-to-pause-for-a-period-of-time
function wait(seconds: number) {
    let waitTill = new Date(new Date().getTime() + seconds * 1000);
    while(waitTill > new Date()){}
}
