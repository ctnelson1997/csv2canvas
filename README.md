Submits grades from a CSV file to Canvas.

A secret token underneath `./includes/token.secret` must be provided. This is an Auth token generated through/by Canvas. An additional grades file undernath `./includes/grades.csv` must be provided. This is a CSV in which the first column is the group number, and alternating columns are scores and comments.

Some specifics are hard coded in. This program assumes that each person belongs to a team who's last token is their group number and that there are 2 pages of students (101-200 students) to fetch. Additionally, values for the course number, assignment number, and rubric are hardcoded. These are non-sensitive values that will differ from assignment to assignment. Code is provided as-is for reference only.
